const container = document.querySelector('.container');
const text = document.querySelector('#text');
//Assigning the container div and the text to variables to be able to change them with JavaScript

const totalTime = 7500;
const breatheTime = (totalTime / 5) * 2;
const holdTime = (totalTime / 5);
//Assigning the total, breathe and hold times. Starting with 7.5 seconds and then changing the breathe and hold times according to the total time

breatheAnimation()
//Running the breatheAnimation as soon as the page loads

function breatheAnimation(){
	text.innerHTML = 'Breathe In'
	container.className = 'container grow'
	//Changing the innerHTML of text to "Breath In" and adding the class "grow" to the container div to change the size of the circle

	//Takes in a function and then takes in a time to wait before executing function, can do another time out within that too
	setTimeout(() => {
		text.innerText = 'Hold'

		setTimeout(() => {
			text.innerText = 'Breathe Out'
			container.className = 'container shrink'
			//Adding the text "Breathe Out" to show when the container is shrinking in size due to the class shrink being added
		}, holdTime)
		//Setting the time out for this function equal to the holdTime as calculated above

	}, breatheTime)
}	//Setting the text to change after the time of breathTime

setInterval(breatheAnimation, totalTime);
//Run the function breatheAnimation every 7.5s or however much time is passed into const totalTime

